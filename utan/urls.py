"""utan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from investasi.views import (
    HomePageView,
    DetailProject,
    UserView,
    Invoice,
    Transaksi
)
from userapp.views import (
    LoginUser,
    DaftarUser,
    Logout
)

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='homepageview'),
    url(r'^login/$', LoginUser.as_view(), name='login'),
    url(r'^logout/$', Logout , name='logout'),
    url(r'^daftar/$', DaftarUser.as_view(), name='daftaruser'),
    url(r'^profile/$', UserView.as_view(), name='userprofile'),
    url(r'^invoice/$', Invoice.as_view(), name='Invoice'),
                       
    url(r'^detailproject/(?P<slug>[\w\d\-\.]+)$', DetailProject.as_view(), name='detailproject'),
    url(r'^transaksi/(?P<slug>[\w\d\-\.]+)$', Transaksi.as_view(), name='transaksi'),
                       
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                 {'document_root': settings.MEDIA_ROOT}),

)

admin.autodiscover()
urlpatterns += patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
)