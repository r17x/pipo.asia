# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0002_userprofile_no_rek'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyBalance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('money', models.DecimalField(null=True, max_digits=30, decimal_places=0, blank=True)),
                ('investing', models.DecimalField(null=True, max_digits=30, decimal_places=0, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='userprofile',
            name='date_created',
            field=models.DateTimeField(default=django.utils.timezone.now, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='date_modified',
            field=models.DateTimeField(default=datetime.datetime(2018, 4, 11, 7, 17, 39, 938922, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mybalance',
            name='user',
            field=models.OneToOneField(related_name='profile', to='userapp.UserProfile'),
        ),
    ]
