# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='no_rek',
            field=models.DecimalField(null=True, max_digits=30, decimal_places=0, blank=True),
        ),
    ]
