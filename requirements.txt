Django==1.8.19
django-easy-pdf==0.1.1
django-user-agents==0.3.2
html5lib==1.0.1
httplib2==0.11.3
Pillow==5.1.0
pkg-resources==0.0.0
PyPDF2==1.26.0
python-memcached==1.59
reportlab==3.4.0
six==1.11.0
ua-parser==0.8.0
user-agents==1.1.0
webencodings==0.5.1
xhtml2pdf==0.2.2
