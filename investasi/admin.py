from django.contrib import admin
from django.forms import ModelForm
from investasi.models import Project,Member
from userapp.models import UserProfile

class ProjectAdmin(admin.ModelAdmin):  
    fieldExclude = ["id", "date_modified","description","lat","lang","date_created"]
    list_display = [field.name for field in Project._meta.fields if field.name not in fieldExclude]
    search_fields = ('name',)
    ordering = ('-id',) 

admin.site.register(Project, ProjectAdmin)

class MemberAdmin(admin.ModelAdmin):  
    fieldExclude = ["id", "profit","toi"]
    search_fields = ('name',)
    ordering = ('-id',) 

admin.site.register(Member, MemberAdmin)


